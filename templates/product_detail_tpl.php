<link href="magiczoomplus/magiczoomplus.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="css/tab.css" type="text/css" rel="stylesheet" />

<div class="breadcrumb">
    <div class="wapper"><?=$bread->display();?></div>
</div>
<div class="wapper cach_top">
    <div class="row">
        <div class="col-md-9 col-sm-12 col-xs-12 col-pull-l">
            <div class="title_page_"><p>Thông tin chi tiết</p></div>
            <div class="box_container">
                <div class="wap_pro clearfix">
                    <div class="zoom_slick">
                        <div class="slick2">
                            <a data-zoom-id="Zoom-detail" id="Zoom-detail" class="MagicZoom" href="<?php if($row_detail['photo'] != NULL)echo _upload_sanpham_l.$row_detail['photo'];else echo 'images/noimage.gif';?>" title="<?=$row_detail['ten']?>"><img class='cloudzoom' src="<?php if($row_detail['photo'] != NULL)echo _upload_sanpham_l.$row_detail['photo'];else echo 'images/noimage.gif';?>" /></a>
                            <?php $count=count($hinhthem); if($count>0) {?>
                            <?php for($j=0,$count_hinhthem=count($hinhthem);$j<$count_hinhthem;$j++){?>
                                <a data-zoom-id="Zoom-detail" id="Zoom-detail" class="MagicZoom" href="<?php if($hinhthem[$j]['photo']!=NULL) echo _upload_hinhthem_l.$hinhthem[$j]['photo']; else echo 'images/noimage.gif';?>" title="<?=$row_detail['ten']?>" ><img src="<?php if($hinhthem[$j]['photo']!=NULL) echo _upload_hinhthem_l.$hinhthem[$j]['photo']; else echo 'images/noimage.gif';?>" /></a>
                            <?php }} ?>
                        </div>
                        <?php $count=count($hinhthem); if($count>0) {?>
                        <div class="slick">
                            <p><img src="thumb/100x80x2x90/<?php if($row_detail['photo'] != NULL)echo _upload_sanpham_l.$row_detail['photo'];else echo 'images/noimage.gif';?>" /></p>
                            <?php for($j=0,$count_hinhthem=count($hinhthem);$j<$count_hinhthem;$j++){?>
                                <p><img src="thumb/100x80x2x90/<?php if($hinhthem[$j]['thumb']!=NULL) echo _upload_hinhthem_l.$hinhthem[$j]['thumb']; else echo 'images/noimage.gif';?>" /></p>
                            <?php } ?>
                        </div>
                        <?php } ?>
                    </div>
                
                    <ul class="product_info">
                        <li class="ten"><h1><?=$row_detail['ten']?></h1></li>
                        <li class="gia"><?php if($row_detail['gia'] != 0)echo number_format($row_detail['gia'],0, ',', '.').' đ';else echo 'Liên hệ'; ?></li>
                        <li><b><?=_luotxem?>:</b> <span><?=$row_detail['luotxem']?></span></li>
                        <?php if($row_detail['mota'] != '') { ?><li><?=$row_detail['mota']?></li><?php } ?>
                        <li><div class="addthis_native_toolbox"></div></li>
                    </ul> 
                </div>
                <div id="tabs">
                    <ul id="ultabs">
                        <li data-vitri="0"><?=_thongtinsanpham?></li>
                    </ul>
                    <div style="clear:both"></div>
                    <div id="content_tabs">
                        <div class="tab">
                            <?=$row_detail['noidung']?>
                        </div>
                         <div class="fb-comments" data-href="<?=getCurrentPageURL()?>" data-numposts="5" data-width="100%"></div>
                    </div>
                </div>
            </div>
            <?php if(count($product)>0) { ?>
                <div class="title_page_"><h2>Sản phẩm liên quan</h2></div>
                <div class="show_product row1">
                <?php foreach ($product as $v) {?>
                    <div class="col-md-4 col-sm-3 col-xs-6 col-pro">
                        <div class="pad_pro">
                            <a href="<?=$v['tenkhongdau']?>">
                                <img src="thumb/272x272x2x90/<?=_upload_sanpham_l.$v['photo']?>" alt="<?=$v['ten']?>">
                            </a>
                            <div class="info-pro">
                                <h3>
                                    <a href="<?=$v['tenkhongdau']?>"><?=$v['ten']?></a>
                                </h3>
                                <div class="gia-pro">
                                    Giá:<span><?php if($v['gia']!=0) echo number_format($v['gia'],0, ',', '.').' đ'; else echo 'Liên hệ'; ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php }?>
                </div>
                <div class="clear"></div>
                <div class="pagination"><?=pagesListLimitadmin($url_link , $totalRows , $pageSize, $offset)?></div>
            <?php }?>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12 col-pull-r">
            <?php include _template."layout/right.php";?>
        </div>
    </div>
</div>
