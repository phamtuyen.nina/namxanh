<div class="bando">
    <?=$company['googlemap']?>
</div>
<div class="breadcrumb">
    <div class="wapper"><?=$bread->display();?></div>
</div>
<section class="section-block customer-section">
    <div class="container">
        <div class="section-content">
            <div style="margin-bottom: 30px">
                 <?=$company_contact['noidung'];?>
            </div>
            <form name="frm" class="frm" action="lien-he.html" class="frm-booking-step1" method="post" novalidate="novalidate">
          <div class="wrap-form inquiry-booking-form select-services-form">
              <h2 class="form-title"><?=_lienhe?></h2>
              <div class="row">
                  <div class="col-xs-12 col-sm-4">
                       <div class="form-group">
                          <label class="control-label"><?=_hovaten?></label>
                          <input class="form-control" type="text"  id="ten_lienhe" name="ten_lienhe">
                      </div>
                  </div>
                <div class="col-xs-12 col-sm-4">
                      <div class="form-group">
                          <label class="control-label"><?=_diachi?></label>
                          <input class="form-control"  type="text" id="diachi_lienhe" name="diachi_lienhe">
                      </div>
                  </div>
                
                <div class="col-xs-12 col-sm-4">
                      <div class="form-group">
                          <label class="control-label"><?=_dienthoai?></label>
                          <input class="form-control"  name="dienthoai_lienhe" type="text" id="dienthoai_lienhe">
                      </div>
                  </div>

                  
                  
              </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                      <div class="form-group">
                          <label class="control-label">Email</label>
                          <input class="form-control" name="email_lienhe" type="text" id="email_lienhe">
                      </div>
                  </div>
                  <div class="col-xs-12 col-sm-6">
                      <div class="form-group">
                          <label class="control-label"><?=_chude?></label>
                          <input class="form-control" name="tieude_lienhe" type="text" id="tieude_lienhe">
                      </div>
                  </div>
                </div>
              <div class="row">
                  <div class="col-xs-12 col-sm-8">
                      <div class="form-group">
                          <label class="control-label"><?=_noidung?></label>
                          <textarea class="form-control" name="noidung_lienhe" id="noidung_lienhe"></textarea>
                      </div>
                  </div>
                   <input type="hidden" name="recaptcha_response" id="recaptchaResponse">
                  <div class="col-xs-12 col-sm-4">
                      <div class="form-group">
                          <button class="btn btn-default btn-send click_ajax" type="button" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Processing">
                              <i class="fa fa-paper-plane-o" aria-hidden="true"></i>
                              <span><?=_gui?></span>
                          </button>
                      </div>
                  </div>
              </div>
          </div>
        </form>
        </div>
    </div>
</section>
