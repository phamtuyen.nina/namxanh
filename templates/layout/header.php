<?php
 	error_reporting(0);
	$d->reset();
	$sql_banner = "select photo$lang as photo from #_background where type='logo' limit 0,1";
	$d->query($sql_banner);
	$row_logo = $d->fetch_array();
		
?>
<div class="nav_top">
	<div class="wapper clearfix">
		<p class="p-em"><i class="fa fa-envelope" aria-hidden="true"></i> <?=$company['email']?></p>
		<p class="p-dt"><i class="fa fa-mobile" aria-hidden="true"></i> <?=$company['dienthoai']?></p>
		<p class="p-lk">
			<?php if(!empty($company['facebook'])){ ?>
				<a href="<?=$company['facebook']?>" target="_blank">
					<i class="fa fa-facebook-square" aria-hidden="true"></i>
				</a>
			<?php }?>
			<?php if(!empty($company['youtube'])){ ?>
				<a href="<?=$company['youtube']?>" target="_blank">
					<i class="fa fa-youtube-square" ></i>
				</a>
			<?php }?>
			<?php if(!empty($company['google'])){ ?>
				<a href="<?=$company['google']?>" target="_blank">
					<i class="fa fa-instagram"></i>
				</a>
			<?php }?>
		</p>
	</div>
</div>
<div class="head_l">
	<div class="wapper clearfix">
		<div class="logo">
			<a href="">
				<img src="<?=_upload_hinhanh_l.$row_logo['photo']?>" alt="<?=$company['ten']?>">
			</a>
		</div>
		<div class="menu">
			<div id="menu">
		        <?php include _template."layout/menu_top.php";?>
		    </div>
		</div>
	</div>
</div>