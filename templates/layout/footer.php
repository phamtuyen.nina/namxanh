<?php	

	$d->reset();
	$sql_contact = "select noidung$lang as noidung from #_about where type='footer' limit 0,1";
	$d->query($sql_contact);
	$company_contact = $d->fetch_array();

    $d->reset();
    $sql = "select mota$lang as mota from #_about where type='about' limit 0,1";
    $d->query($sql);
    $about_contact = $d->fetch_array();

    $d->reset();
    $sql = "select id,ten$lang as ten,tenkhongdau from #_news where type='chinh-sach' and hienthi=1 order by stt,id desc";
    $d->query($sql);
    $chinhsach = $d->result_array();

    $d->reset();
    $sql_banner = "select photo$lang as photo from #_background where type='logo' limit 0,1";
    $d->query($sql_banner);
    $row_logo = $d->fetch_array();    
?>
<div class="wapper">
    <div class="row">
        <div class="col-md-4 col-main1">
            <div class="ll_logo_ft">
                <a href="">
                    <img src="<?=_upload_hinhanh_l.$row_logo['photo']?>" alt="<?=$company['ten']?>">
                </a>
            </div>
            <div class="mm_about_ft"><?=$about_contact['mota']?></div>
            <div class="mm_tt_ft">
                <ul>
                    <li><i class="fa fa-mobile" aria-hidden="true"></i> <span>Hotline:</span> <?=$company['dienthoai']?></li>
                    <li><i class="fa fa-envelope" aria-hidden="true"></i> <span>Email:</span> <?=$company['email']?></li>
                    <li><i class="fa fa-map-marker" aria-hidden="true"></i> <span>Địa chỉ:</span> <?=$company['diachi']?></li>
                    <li><i class="fa fa-clock-o" aria-hidden="true"></i> <span>Giờ làm:</span> <?=$company['skype']?></li>
                </ul>
            </div>
        </div>
        <div class="col-md-2 col-main2">
            <p>Thông tin</p>
            <ul>
                <li><a href="gioi-thieu">Giới thiệu</a></li>
                <li><a href="hop-tac-kinh-doanh">Hợp tác kinh doanh</a></li>
                <li><a href="huong-dan-mua-hang">Hướng dẫn mua hàng</a></li>
                <li><a href="hoi-dap">Hỏi đáp</a></li>
                <li><a href="lien-he">Liên hệ</a></li>
            </ul>
        </div>
        <div class="col-md-2 col-main2">
            <p>Chính sách</p>
            <ul>
                <?php foreach ($chinhsach as $key => $v) {?>
                <li>
                    <a href="<?=$v['tenkhongdau']?>"><?=$v['ten']?></a>
                </li>
                <?php }?>
            </ul>
        </div>
        <div class="col-md-4 col-main3">
            <p class="t_main3">Đăng ký bản tin</p>
            <div class="nhantin">
                <p>Ưu tiên nhận các bài viết chia sẽ, những sự kiện hấp dẫn và voucher giảm giá từ LaDo Mushrooms Farm dành riêng cho bạn.</p>
                 <?php include _template."layout/dangkynhantin.php";?>
            </div>
            <div class="fb-page" data-href="<?=$company['fanpage']?>" data-tabs="" data-width="380" data-height="150" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"></div>
        </div>
    </div>
</div>
<div class="copy-right">
	<div class="wapper">
    	<?=$company['yahoo']?>
       
    </div>
</div>