<?php $data_json = explode('0', preg_replace('/[^0-9]/','',$company['dienthoai']),2);?>
<script type="application/ld+json">
    {
      "@context" : "https://schema.org",
      "@type" : "Organization",
      "name" : "<?=$row_setting['ten']?>",
      "url" : "<?=$http.$config_url?>",
      "sameAs" : [
          <?php if(!empty($company['facebook'])){ ?>
            "<?=$company['facebook']?>"
          <?php }?>
          <?php if(!empty($company['youtube'])){ ?>
            ,"<?=$company['youtube']?>"
          <?php }?>
          <?php if(!empty($company['google'])){ ?>
            ,"<?=$company['google']?>"
          <?php }?>
       ],
      "address": {
        "@type": "PostalAddress",
        "streetAddress": "<?=$row_setting['diachi']?>",
        "addressRegion": "Ho Chi Minh",
        "postalCode": "70000",
        "addressCountry": "vi"
      },
      "contactPoint": [
        { "@type": "ContactPoint",
          "telephone": "+84<?=$data_json[1]?>",
          "contactType": "customer service"
        }
      ]
    }
</script>

<?php if($template=='product_detail') {?>
<script type="application/ld+json">
{
  "@context": "https://schema.org/",
  "@type": "Product",
  "name": "<?=$row_detail['ten']?>",
  "image": [
    "<?=$http.$config_url?>/<?=_upload_sanpham_l.$row_detail['photo']?>"
    <?php foreach ($hinhthem as $v) {?>
    ,"<?=$http.$config_url?>/<?=_upload_hinhthem_l.$v['photo']?>"
    <?php }?>
   ],
  "description": "<?=trim(strip_tags($row_detail['mota']))?>",
  "sku": "<?=$row_detail['masp']?>",
  "mpn": "<?=$row_detail['masp']?>",
  "brand": {
    "@type": "Thing",
    "name": "<?=$title_new1['ten']?>"
  },
  "aggregateRating": {
    "@type": "AggregateRating",
    "ratingValue": "4.4",
    "reviewCount": "<?=$tintuc_detail['id']*14+date('m')?>"
  },
  "offers": {
    "@type": "Offer",
    "url": "https://example.com/anvil",
    "priceCurrency": "VND",
    "price": "<?=$row_detail['gia']?>",
    "priceValidUntil": "<?=date('Y-m-d',($row_detail['ngaytao']+(365*86400)))?>",
    "itemCondition": "https://schema.org/UsedCondition",
    "availability": "https://schema.org/InStock"
  }
}  
</script>
<?php } ?>
<?php if($template=='news_detail'){?>
 <script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "NewsArticle",
  "mainEntityOfPage": {
    "@type": "WebPage",
    "@id":"<?=getCurrentPageURL()?>"
  },
  "headline": "<?=$tintuc_detail['ten']?>",
  "image": [
    "<?=$http.$config_url?>/<?=_upload_tintuc_l.$tintuc_detail['photo']?>"
   ],
  "datePublished": "<?=date('c',$tintuc_detail['ngaytao'])?>",
  "dateModified": "<?=date('c',$tintuc_detail['ngaysua'])?>",
  "author": {
    "@type": "Person",
    "name": "<?=$company['ten']?>"
  },
   "publisher": {
    "@type": "Organization",
    "name": "<?=$company['ten']?>",
    "logo": {
      "@type": "ImageObject",
      "name": "<?=$company['ten']?>",
      "width": "329",
      "height": "122",
      "url": "<?=$http.$config_url?>/<?=_upload_hinhanh_l.$row_logo['photo']?>"
    }
  },
  "description": "<?=trim(strip_tags($tintuc_detail['mota']))?>",
  "aggregateRating" : {
      "@type" : "AggregateRating",
      "ratingValue" : "4.8",
      "bestRating" : "5",
      "ratingCount" : "<?=$tintuc_detail['id']*14+date('m')?>"
    }
}
</script> 
<?php }?>
<?php if(!empty($schema_faq)){ ?>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [<?php foreach ($schema_faq as $k => $v) {?>{
    "@type": "Question",
    "name": "<?=$v['ten']?>",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "<?=$v['noidung']?>"
    }
  }<?php if($k<count($schema_faq)-1){echo ',';}}?>]
  }
</script>
<?php }?>
<?php if($type=='about'){?>
<script type="application/ld+json">
  {
    "@context": "https://schema.org",
    "@type": "NewsArticle",
    "mainEntityOfPage": {
      "@type": "WebPage",
      "@id":"<?=getCurrentPageURL()?>"
    },
    "headline": "<?=$tintuc_detail['ten']?>",
    "image": [
      "<?=$http.$config_url?>/<?=_upload_tintuc_l.$tintuc_detail['photo']?>"
     ],
    "datePublished": "<?=date('c',$tintuc_detail['ngaytao'])?>",
    "dateModified": "<?=date('c',$tintuc_detail['ngaysua'])?>",
    "author": {
      "@type": "Person",
      "name": "<?=$company['ten']?>"
    },
    "publisher": {
      "@type": "Organization",
      "name": "<?=$company['ten']?>",
      "logo": {
        "@type": "ImageObject",
        "name": "<?=$company['ten']?>",
        "width": "329",
        "height": "122",
        "url": "<?=$http.$config_url?>/<?=_upload_hinhanh_l.$row_logo['photo']?>"
      }
    },
    "description": "<?=trim(strip_tags($tintuc_detail['mota']))?>",
    "aggregateRating" : {
        "@type" : "AggregateRating",
        "ratingValue" : "5",
        "bestRating" : "5",
        "ratingCount" : "<?=$tintuc_detail['id']*14+date('m')?>"
    }
  }
</script>
<?php }?>