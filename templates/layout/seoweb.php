<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="SHORTCUT ICON" href="<?=_upload_hinhanh_l.$company['faviconthumb']?>" type="image/x-icon" />
<META NAME="ROBOTS" CONTENT="<?=$config['nobots']?>" />
<link rel="canonical" href="<?=getCurrentPageURL_CANO()?>" />
<meta name="author" content="<?=$company['ten']?>" />
<meta name="copyright" content="<?=$company['ten']?> [<?=$company['email']?>]" />
<title><?php if($title!='')echo $title;else echo $company['title'];?></title>
<meta name="keywords" content="<?php if($keywords!='')echo $keywords;else echo $company['keywords'];?>" />
<meta name="description" content="<?php if($description!='')echo $description;else echo $company['description'];?>" />
<meta name="DC.title" content="<?php if($title!='')echo $title;else echo $company['title'];?>" />
<meta name="geo.region" content="VN" />
<meta name="geo.placename" content="<?=$company['diachi']?>" />
<meta property="og:image:alt" content="<?php if($title_facebook!=''){echo $title_facebook;}else{echo $company['title'];}?>" />
<meta name="DC.identifier" content="<?=$http.$config_url?>" />
<meta property="og:image" content="<?php if($images_facebook!=''){echo $images_facebook;}else{echo $company['images'];}?>" />
<meta property="og:title" content="<?php if($title_facebook!=''){echo $title_facebook;}else{echo $company['title'];}?>" />
<meta property="og:url" content="<?=getCurrentPageURL();?>" />
<meta property="og:site_name" content="<?=$http.$config_url?>" />
<meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
<meta property="og:description" content="<?php if($description_facebook!=''){echo $description_facebook;}else{echo $company['description'];}?>" />
<?php if(!empty($type_og)){ ?>
<meta property="og:type" content="<?=$type_og?>" />
<?php }?>
<meta property="og:site_name" content="<?=$company['ten']?>" />
