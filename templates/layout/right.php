<?php
	$d->reset();
	$sql_product_danhmuc="select ten$lang as ten,tenkhongdau,id from #_product_danhmuc where hienthi=1 and type='san-pham' order by stt,id desc";
	$d->query($sql_product_danhmuc);
	$product_danhmuc=$d->result_array();

    $d->reset();
    $sql="select ten$lang as ten,tenkhongdau,id from #_news_danhmuc where hienthi=1 and type='blog-chia-se' order by stt,id desc";
    $d->query($sql);
    $blog_danhmuc=$d->result_array(); 
?>
<div class="bx_bb">
	<div class="wap_right">
		<h3>Menu sản phẩm</h3>
		<div class="box_ul">
			<ul>
				<?php foreach ($product_danhmuc as $v) {?>
				<li>
					<a href="<?=$v['tenkhongdau']?>"><i class="fa fa-angle-double-right" aria-hidden="true"></i> <h2><?=$v['ten']?></h2></a> <span>(<?=getCount($v['id'],'table_product')?>)</span>
				</li>
				<?php }?>
			</ul>
		</div>
	</div>
</div>

<div class="bx_bb">
	<div class="wap_right">
		<h3>Menu Blog</h3>
		<div class="box_ul">
			<ul>
				<?php foreach ($blog_danhmuc as $v) {?>
				<li>
					<a href="<?=$v['tenkhongdau']?>"><i class="fa fa-angle-double-right" aria-hidden="true"></i> <h2><?=$v['ten']?></h2> </a><span>(<?=getCount($v['id'],'table_news')?>)</span>
				</li>
				<?php }?>
			</ul>
		</div>
	</div>
</div>