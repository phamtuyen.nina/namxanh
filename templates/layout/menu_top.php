<?php
	$d->reset();
	$sql_product_danhmuc="select ten$lang as ten,tenkhongdau,id from #_product_danhmuc where hienthi=1 and type='san-pham' order by stt,id desc";
	$d->query($sql_product_danhmuc);
	$product_danhmuc=$d->result_array();

    $d->reset();
    $sql="select ten$lang as ten,tenkhongdau,id from #_news_danhmuc where hienthi=1 and type='blog-chia-se' order by stt,id desc";
    $d->query($sql);
    $blog_danhmuc=$d->result_array();    
?>

<ul itemscope itemtype="http://www.schema.org/SiteNavigationElement">	
    <li itemprop="name"><a itemprop="url" class="<?php if((!isset($_REQUEST['com'])) or ($_REQUEST['com']==NULL) or $_REQUEST['com']=='index') echo 'active'; ?>" href=""><?=_trangchu?></a></li>
    <li itemprop="name"><a itemprop="url" class="<?php if($_REQUEST['com'] == 'gioi-thieu') echo 'active'; ?>" href="gioi-thieu"><?=_gioithieu?></a></li>
    <li itemprop="name" ><a itemprop="url" class="<?php if($_REQUEST['com'] == 'san-pham') echo 'active'; ?>" href="san-pham"><?=_sanpham?></a>
    	<ul>
			<?php for($i = 0;$i<count($product_danhmuc); $i++){ 
			
				$d->reset();
				$sql_product_list="select ten$lang as ten,tenkhongdau,id from #_product_list where hienthi=1 and id_danhmuc='".$product_danhmuc[$i]['id']."' order by stt,id desc";
				$d->query($sql_product_list);
				$product_list=$d->result_array();			
			?>
            <li><a href="<?=$product_danhmuc[$i]['tenkhongdau']?>"><?=$product_danhmuc[$i]['ten']?></a>
                
                <?php if(count($product_list)>0){?>
                <ul>
					
                     <?php for($j = 0;$j < count($product_list); $j++){ 
					 
					$d->reset();
					$sql_product_list="select ten$lang as ten,tenkhongdau,id from #_product_cat where hienthi=1 and id_list='".$product_list[$j]['id']."' order by stt,id desc";
					$d->query($sql_product_list);
					$product_cat=$d->result_array();
					 
					 ?>
                            <li><a href="<?=$product_list[$j]['tenkhongdau']?>"><?=$product_list[$j]['ten']?></a>
                            
                            <?php if(count($product_cat)>0){?>
                            <ul>                            
								<?php for($k = 0;$k < count($product_cat); $k++){ ?>
                                    <li><a href="<?=$product_cat[$k]['tenkhongdau']?>"><?=$product_cat[$k]['ten']?></a></li>
                                <?php } ?>
                            </ul>
                            <?php } ?>
                                
                            </li>
                     <?php } ?>
                 </ul>
                 <?php } ?>
                </li>
                <?php } ?>
            </ul>	
    </li>
    <li itemprop="name"><a itemprop="url" class="<?php if($_REQUEST['com'] == 'blog-chia-se') echo 'active'; ?>" href="blog-chia-se">Blog chia sẻ</a>
        <ul>
            <?php for($i = 0;$i<count($blog_danhmuc); $i++){?>
            <li><a href="<?=$blog_danhmuc[$i]['tenkhongdau']?>"><?=$blog_danhmuc[$i]['ten']?></a></li>
            <?php } ?>
        </ul>
    </li>
    <li itemprop="name"><a itemprop="url" class="<?php if($_REQUEST['com'] == 'quy-trinh') echo 'active'; ?>" href="quy-trinh">Quy trình</a></li>
    <li itemprop="name"><a itemprop="url" class="<?php if($_REQUEST['com'] == 'lien-he') echo 'active'; ?>" href="lien-he"><?=_lienhe?></a></li>
</ul>
<div id="search">
    <input type="text" name="keyword" id="keyword" onKeyPress="doEnter(event,'keyword');" value="<?=_nhaptukhoatimkiem?>..."  onclick="if(this.value=='<?=_nhaptukhoatimkiem?>...'){this.value=''}" onblur="if(this.value==''){this.value='<?=_nhaptukhoatimkiem?>...'}">
    <i class="fa fa-search" aria-hidden="true" onclick="onSearch(event,'keyword');"></i>
</div>

