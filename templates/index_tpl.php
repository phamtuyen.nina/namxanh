<?php 
    $d->reset();
    $sql = "select id,ten$lang as ten,tenkhongdau,mota$lang as mota,photo from #_news where type='baiviet' and hienthi=1 order by stt,id desc";
    $d->query($sql);
    $baivietvenam = $d->result_array();

    $d->reset();
    $sql = "select id,ten$lang as ten,tenkhongdau,mota$lang as mota,photo,ngaytao from #_news where type='blog-chia-se' and noibat=1 and hienthi=1 order by stt,id desc";
    $d->query($sql);
    $blog = $d->result_array();  

    $d->reset();
    $sql = "select ten$lang as ten,link,photo,mota$lang as mota from #_slider where hienthi=1 and type='visao' order by stt,id desc";
    $d->query($sql);
    $visao=$d->result_array();  

    $d->reset();
    $sql = "select id,ten$lang as ten,tenkhongdau,mota$lang as mota,photo,ngaytao from #_news where type='quy-trinh' and hienthi=1 order by stt,id desc";
    $d->query($sql);
    $quytrinh = $d->result_array();  

    $d->reset();
    $sql = "select ten$lang as ten,link,photo,mota$lang as mota from #_slider where hienthi=1 and type='ykien' order by stt,id desc";
    $d->query($sql);
    $ykien = $d->result_array();

	$d->reset();
	$sql = "select noidung$lang as noidung from #_about where type='hoptac' limit 0,1";
	$d->query($sql);
	$hoptac_contact = $d->fetch_array();  

	$d->reset();
	$sql = "select noidung$lang as noidung from #_about where type='hoidap' limit 0,1";
	$d->query($sql);
	$hoidap_contact = $d->fetch_array();  	
	                
?>

<div class="wap_baiviet">
<?php foreach ($baivietvenam as $key => $v) {?>
	<div class="wap_bv">
		<div class="wapper">
			<h2 class="tt_bv"><?=$v['ten']?></h2>
			<div class="row1">
				<div class="col-md-6 col-sm-6 col-xs-6 col-img-bv">
					<img class="lazy" data-src="<?=_upload_tintuc_l.$v['photo']?>" alt="<?=$v['ten']?>">
				</div>
				<div class="col-md-6 col-sm-6 col-xs-6 col-nd-bv">
					<div class="content_bvn">
						<?=$v['mota']?>
					</div>
					<p class="xemthemx">
						<a href="<?=$v['tenkhongdau']?>">Tìm hiểu ngay</a>
					</p>
				</div>
			</div>
		</div>
	</div>
<?php }?>
</div>
<div class="wap_visao">
	<div class="wapper">
		<div class="title_page_">
			<h2>Vì sao chọn chúng tôi</h2>
		</div>
		<div class="row1">
			<?php foreach ($visao as $key => $v) {?>
			<div class="col-md-3 col-sm-6 col-xs-6 col-vs text-center">
				<div class="pad_vs">
					<p><img class="lazy" data-src="<?=_upload_hinhanh_l.$v['photo']?>" alt="<?=$v['ten']?>"></p>
					<h5><?=$v['ten']?></h5>
					<div class="mm_vs"><?=$v['mota']?></div>
				</div>
			</div>
			<?php }?>
		</div>
	</div>
</div>
<div class="wap_quytrinh">
	<div class="title_page_">
		<h2>Quy trình hoạt động</h2>
	</div>
	<div class="all_qt">
	<?php foreach ($quytrinh as $key => $v) {?>
		<div class="bx_qcc">
			<a href="<?=$v['tenkhongdau']?>">
				<img class="lazy" data-src="thumb/640x390x1x90/<?=_upload_tintuc_l.$v['photo']?>" alt="<?=$v['ten']?>">
				<span><?=$v['ten']?></span>
			</a>
		</div>
		<div class="bx_qcc">
			<a href="<?=$v['tenkhongdau']?>">
				<img class="lazy" data-src="thumb/640x390x1x90/<?=_upload_tintuc_l.$v['photo']?>" alt="<?=$v['ten']?>">
				<span><?=$v['ten']?></span>
			</a>
		</div>
		<div class="bx_qcc">
			<a href="<?=$v['tenkhongdau']?>">
				<img class="lazy" data-src="thumb/640x390x1x90/<?=_upload_tintuc_l.$v['photo']?>" alt="<?=$v['ten']?>">
				<span><?=$v['ten']?></span>
			</a>
		</div>
		<div class="bx_qcc">
			<a href="<?=$v['tenkhongdau']?>">
				<img class="lazy" data-src="thumb/640x390x1x90/<?=_upload_tintuc_l.$v['photo']?>" alt="<?=$v['ten']?>">
				<span><?=$v['ten']?></span>
			</a>
		</div>
		<div class="bx_qcc">
			<a href="<?=$v['tenkhongdau']?>">
				<img class="lazy" data-src="thumb/640x390x1x90/<?=_upload_tintuc_l.$v['photo']?>" alt="<?=$v['ten']?>">
				<span><?=$v['ten']?></span>
			</a>
		</div>
		<div class="bx_qcc">
			<a href="<?=$v['tenkhongdau']?>">
				<img class="lazy" data-src="thumb/640x390x1x90/<?=_upload_tintuc_l.$v['photo']?>" alt="<?=$v['ten']?>">
				<span><?=$v['ten']?></span>
			</a>
		</div>
	<?php }?>
	</div>
</div>
<div class="wap_hoptac">
	<div class="wapper wrap">
		 <div class="row1">
		 	<div class="col-md-9 col-sm-12 col-xs-12 col-text-hoptac">
		 		<div class="cc_hoptac">
		 			<?=$hoptac_contact['noidung']?>
		 		</div>
		 	</div>
		 	<div class="col-md-3 col-sm-12 col-xs-12 col-text-hoptac-link text-center">
		 		<a href="hop-tac-kinh-doanh"><i class="fa fa-address-book" aria-hidden="true"></i> Hợp tác</a>
		 	</div>
		 </div>
	</div>
</div>
<div class="wap_ykienkh">
	<div class="wapper">
		<div class="title_page_">
			<h2>Khách hàng nói gì về chúng tôi</h2>
		</div>
		<div class="row">
			<div class="slick_ykien">
				<?php foreach ($ykien as $key => $v) {?>
				<div class="col-xs-6">
					<div class="pad_ykien">
						<p>
							<img src="thumb/100x100x1x90/<?=_upload_hinhanh_l.$v['photo']?>" alt="<?=$v['ten']?>">
						</p>
						<div class="mm_ykien"><?=$v['mota']?></div>
						<h6><?=$v['ten']?></h6>
					</div>
				</div>
				
				<?php }?>
			</div>
		</div>
	</div>
</div>
<div class="wap_blog">
	<div class="wapper">
		<div class="title_page_">
			<h2>Blog chia sẻ</h2>
		</div>
		<div class="row">
			<div class="slick_chiase">
				<?php foreach ($blog as $key => $v) {?>
				<div class="col-xs-3">
					<div class="bad_blog">
						<div class="img_blog">
							<a href="<?=$v['tenkhongdau']?>">
								<img onError="this.src='http://placehold.it/420x250';" src="thumb/420x250x1x90/<?=_upload_tintuc_l.$v['photo']?>" alt="<?=$v['ten']?>">
							</a>
						</div>
						<div class="info_tin">
							<h4>
								<a href="<?=$v['tenkhongdau']?>"><?=$v['ten']?></a>
							</h4>
							<p class="creat_date"><i class="fa fa-calendar" aria-hidden="true"></i> <?=date('d/m/Y H:i:s',$v['ngaytao'])?></p>
							<div class="content_blog"><?=catchuoi(trim(strip_tags($v['mota'])),150)?></div>
							<p class="xemthem_blog"><a href="<?=$v['tenkhongdau']?>">Chi tiết</a></p>
						</div>
					</div>
				</div>

				<?php }?>
			</div>
		</div>
	</div>
</div>

<div class="wap_hoidap">
	<div class="wapper wrap">
		 <div class="row1">
		 	<div class="col-md-9 col-sm-12 col-xs-12 col-text-hoidap">
		 		<div class="cc_hoidap">
		 			<?=$hoidap_contact['noidung']?>
		 		</div>
		 	</div>
		 	<div class="col-md-3 col-sm-12 col-xs-12 col-text-hoidap-link text-center">
		 		<a href="hoi-dap"><i class="fa fa-address-book" aria-hidden="true"></i> Xem FAQs</a>
		 	</div>
		 </div>
	</div>
</div>
<script type="application/ld+json"> 
{
	"@context": "http://schema.org",
	"@type": "BreadcrumbList",
	"itemListElement": [
		{
		"@type": "ListItem",
		"position": 1,
		"name": "Trang chủ",
		"item": "<?=$http.$config_url?>"
		},
		{
		"@type": "ListItem",
		"position": 2,
		"name": "✅(Đã xác minh)",
		"item": "<?=$http.$config_url?>"
		}
	]
}
</script>