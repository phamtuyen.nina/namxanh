<?php 

	$d->reset();
	$sql_tinmoi = "select id,ten$lang as ten,tenkhongdau,noidung$lang as noidung from #_news where type='".$type."' and hienthi=1 order by stt,id desc";
	$d->query($sql_tinmoi);
	$tinmoi = $d->result_array();
 ?>
<div class="breadcrumb">
	<div class="wapper"><?=$bread->display();?></div>
</div>
<div class="wapper cach_top">
	<div class="row">
		<div class="col-md-9 col-sm-12 col-xs-12 col-pull-l">
			<div class="title_page_"><h1><?=$title_cat?></h1></div>
			<div class="wap_hoiddap">
				<?php foreach ($tinmoi as $v) {?>
				<div class="tve_faq">
					<div class="tve_faqI">
						<div class="tve_faqB">
							<h4><i class="fa fa-caret-right" aria-hidden="true"></i> <?=$v['ten']?></h4>
						</div>
						<div class="tve_faqC">
							<div class="mm_faq">
								<?=$v['noidung']?>
							</div>
						</div>
					</div>
				</div>
				<?php }?>
			</div>
		</div>
		<div class="col-md-3 col-sm-12 col-xs-12 col-pull-r">
            <?php include _template."layout/right.php";?>
        </div>
	</div>
</div>        