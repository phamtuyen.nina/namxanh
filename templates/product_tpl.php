<div class="breadcrumb">
    <div class="wapper"><?=$bread->display();?></div>
</div>
<div class="wapper cach_top">
    <div class="row">
        <div class="col-md-9 col-sm-12 col-xs-12 col-pull-l">
            <div class="title_page_"><h1><?=$title_cat?></h1></div>
            <div class="show_product row1">
                <?php foreach ($product as $v) {?>
                <div class="col-md-4 col-sm-3 col-xs-6 col-pro">
                    <div class="pad_pro">
                        <a href="<?=$v['tenkhongdau']?>">
                            <img src="thumb/272x272x2x90/<?=_upload_sanpham_l.$v['photo']?>" alt="<?=$v['ten']?>">
                        </a>
                        <div class="info-pro">
                            <h3>
                                <a href="<?=$v['tenkhongdau']?>"><?=$v['ten']?></a>
                            </h3>
                            <div class="gia-pro">
                                Giá:<span><?php if($v['gia']!=0) echo number_format($v['gia'],0, ',', '.').' đ'; else echo 'Liên hệ'; ?></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-3 col-xs-6 col-pro">
                    <div class="pad_pro">
                        <a href="<?=$v['tenkhongdau']?>">
                            <img src="thumb/272x272x2x90/<?=_upload_sanpham_l.$v['photo']?>" alt="<?=$v['ten']?>">
                        </a>
                        <div class="info-pro">
                            <h3>
                                <a href="<?=$v['tenkhongdau']?>"><?=$v['ten']?></a>
                            </h3>
                            <div class="gia-pro">
                                Giá:<span><?php if($v['gia']!=0) echo number_format($v['gia'],0, ',', '.').' đ'; else echo 'Liên hệ'; ?></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-3 col-xs-6 col-pro">
                    <div class="pad_pro">
                        <a href="<?=$v['tenkhongdau']?>">
                            <img src="thumb/272x272x2x90/<?=_upload_sanpham_l.$v['photo']?>" alt="<?=$v['ten']?>">
                        </a>
                        <div class="info-pro">
                            <h3>
                                <a href="<?=$v['tenkhongdau']?>"><?=$v['ten']?></a>
                            </h3>
                            <div class="gia-pro">
                                Giá:<span><?php if($v['gia']!=0) echo number_format($v['gia'],0, ',', '.').' đ'; else echo 'Liên hệ'; ?></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-3 col-xs-6 col-pro">
                    <div class="pad_pro">
                        <a href="<?=$v['tenkhongdau']?>">
                            <img src="thumb/272x272x2x90/<?=_upload_sanpham_l.$v['photo']?>" alt="<?=$v['ten']?>">
                        </a>
                        <div class="info-pro">
                            <h3>
                                <a href="<?=$v['tenkhongdau']?>"><?=$v['ten']?></a>
                            </h3>
                            <div class="gia-pro">
                                Giá:<span><?php if($v['gia']!=0) echo number_format($v['gia'],0, ',', '.').' đ'; else echo 'Liên hệ'; ?></span>
                            </div>
                        </div>
                    </div>
                </div>
                <?php }?>
            </div>
            <div class="clear"></div>
            <div class="pagination"><?=pagesListLimitadmin($url_link , $totalRows , $pageSize, $offset)?></div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12 col-pull-r">
            <?php include _template."layout/right.php";?>
        </div>
    </div>
</div>
