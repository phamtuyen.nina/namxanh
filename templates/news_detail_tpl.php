<div class="breadcrumb">
    <div class="wapper"><?=$bread->display();?></div>
</div>
<div class="wapper cach_top">
    <div class="row">
        <div class="col-md-9 col-sm-12 col-xs-12 col-noidung">
            <div class="w_tieude_tin">
                <h1><?=$title_cat?></h1p>
                <div class="sharethis-inline-share-buttons"></div>
            </div>
            <div class="in_mota">
                <?=$tintuc_detail['mota']?>
            </div>
            <?=$tintuc_detail['noidung']?>
            <br>
            <div class="addthis_native_toolbox"></div>
            <div class="fb-comments" data-href="<?=getCurrentPageURL()?>" data-numposts="5" data-width="100%"></div>
            <?php if(count($tintuc)>0){ ?>
            <div class="ccol--lq col-lq">
                <div class="w_tieude_tin">
                    <p>Các bài viết khác</p>
                </div>
                <ul>
                    <?php foreach($tintuc as $v){ ?>
                    <li>
                        <a href="<?=$com?>/<?=$v['tenkhongdau']?>.html">
                            <i class="fa fa-angle-double-right" aria-hidden="true"></i> <?=$v['ten']?>
                        </a>
                    </li>
                    <?php }?>
                </ul>
            </div>
            <?php }?>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12 col-pull-r">
            <?php include _template."layout/right.php";?>
        </div>
    </div>
</div>
