<div class="logo"> <a href="#" target="_blank" onclick="return false;"> <img src="images/logo.png"  alt="" /> </a></div>
<div class="sidebarSep mt0"></div>
<!-- Left navigation -->
<ul id="menu" class="nav">
  <li class="dash" id="menu1"><a class=" active" title="" href="index.php"><span>Trang chủ</span></a></li>

  <li class="categories_li product_li <?php if($_GET['com']=='product' || $_GET['com']=='order' || $_GET['com']=='excel') echo ' activemenu' ?>" id="menu2"><a href="" title="" class="exp"><span>Sản phẩm</span><strong></strong></a>
   <ul class="sub">     
     	<?php phanquyen_menu('Quản lý danh mục 1','product','man_danhmuc','san-pham'); ?>
        <?php phanquyen_menu('Quản lý sản phẩm','product','man','san-pham'); ?>
        <?php phanquyen_menu('Quản lý Schema FAQ','product','man_list','chemafaq'); ?>
        <?php //phanquyen_menu('Quản lý đơn hàng','order','man',''); ?>
     
    </ul>
  </li>

	    <li class="categories_li news_li <?php if($_GET['com']=='news' && $_GET['type']=='blog-chia-se') echo ' activemenu' ?>" id="menu_tt"><a href="" title="" class="exp"><span>Blog chia sẻ</span><strong></strong></a>
        <ul class="sub">  
            <?php phanquyen_menu('Danh mục cấp 1','news','man_danhmuc','blog-chia-se'); ?>
            <?php phanquyen_menu('Quản lý blog','news','man','blog-chia-se'); ?>         
        </ul>
      </li>

      <li class="categories_li news_li <?php if($_GET['com']=='news' && $_GET['type']!='blog-chia-se') echo ' activemenu' ?>" id="menu_tt1"><a href="" title="" class="exp"><span>Nội dung khác</span><strong></strong></a>
        <ul class="sub">  
            <?php phanquyen_menu('Bài viết về nấm','news','man','baiviet'); ?>         
            <?php phanquyen_menu('Quản lý chính sách','news','man','chinh-sach'); ?>         
            <?php phanquyen_menu('Quản lý quy trình','news','man','quy-trinh'); ?>         
            <?php phanquyen_menu('Hỏi đáp','news','man','hoidap'); ?>         
        </ul>
      </li>
      
      
      <li class="categories_li newsletter_li <?php if($_GET['com']=='newsletter') echo ' activemenu' ?>" id="menu_nt"><a href="" title="" class="exp"><span>Đăng ký nhận tin</span><strong></strong></a>
      	<ul class="sub">
            <?php phanquyen_menu('Quản lý Đăng ký nhận tin','newsletter','man',''); ?>     
        </ul>
      </li>
   
      <li class="categories_li gallery_li <?php if($_GET['com']=='background' || $_GET['com']=='anhnen' || $_GET['com']=='slider' || $_GET['com']=='letruot') echo ' activemenu' ?>" id="menu_qc"><a href="" title="" class="exp"><span>Banner - Quảng cáo</span><strong></strong></a>
      
           <ul class="sub">
     		    <?php phanquyen_menu('Cập nhật logo','background','capnhat','logo'); ?>
            <?php phanquyen_menu('Quản lý slider','slider','man_photo','slider'); ?>
            <?php phanquyen_menu('Vì sao chọn chúng tôi','slider','man_photo','visao'); ?>
            <?php phanquyen_menu('Ý kiến khách hàng','slider','man_photo','ykien'); ?>
     </ul>
     
      </li>


     <li class="categories_li about_li <?php if($_GET['com']=='about' || $_GET['com']=='video') echo ' activemenu' ?>" id="menu_t"><a href="" title="" class="exp"><span>Trang tĩnh</span><strong></strong></a>
    <ul class="sub">
        <?php phanquyen_menu('Giới thiệu','about','capnhat','about'); ?>
        <?php phanquyen_menu('Trại nấm','about','capnhat','trainam'); ?>
        <?php phanquyen_menu('Hợp tác kinh doanh','about','capnhat','hoptac'); ?>
        <?php phanquyen_menu('Hướng dẫn mua hàng','about','capnhat','huongdanmuahang'); ?>
        <?php phanquyen_menu('Cập nhật liên hệ','about','capnhat','lienhe'); ?>
        <?php phanquyen_menu('Cập nhật footer','about','capnhat','footer'); ?>
        <?php phanquyen_menu('Mô tả hợp tác trang chủ','about','capnhat','hoptac'); ?>
        <?php phanquyen_menu('Mô tả hỏi đáp','about','capnhat','hoidap'); ?>
        </ul>
  </li>

     <li class="categories_li setting_li <?php if($_GET['com']=='lkweb' || $_GET['com']=='yahoo' || $_GET['com']=='company' || $_GET['com']=='meta' || $_GET['com']=='user' || $_GET['com']=='video') echo ' activemenu' ?>" id="menu_cp"><a href="" title="" class="exp"><span>Nội dung khác</span><strong></strong></a>
  	<ul class="sub">
    	<?php phanquyen_menu('Video','video','man',''); ?>
        <?php phanquyen_menu('Cập nhật thông tin công ty','company','capnhat',''); ?>
         <li<?php if($_GET['act']=='admin_edit') echo ' class="this"' ?>><a href="index.php?com=user&act=admin_edit">Quản lý Tài Khoản</a></li>
    </ul>
  </li>
</ul>



