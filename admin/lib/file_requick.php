<?php
	$com = (isset($_REQUEST['com'])) ? addslashes($_REQUEST['com']) : "";
	$act = (isset($_REQUEST['act'])) ? addslashes($_REQUEST['act']) : "";
	$d = new database($config['database']);
	
	$d->reset();
	$sql_company = "select lang_default from #_company limit 0,1";
	$d->query($sql_company);
	$company1= $d->fetch_array();
	
	$lang_default = array("","en");
	if(!isset($_SESSION['lang']) or !in_array($_SESSION['lang'], $lang_default))
	{
		$_SESSION['lang'] = $company1['lang_default'];
	}
	$lang=$_SESSION['lang'];
	require_once _source."lang$lang.php";

	$d->reset();
	$sql_company = "select *,ten$lang as ten,diachi$lang as diachi from #_company limit 0,1";
	$d->query($sql_company);
	$company= $d->fetch_array();

	$d->reset();
	$sql = "select photo from #_about where type='about' limit 0,1";
	$d->query($sql);
	$about_1 = $d->fetch_array();

	$a=isset($_SERVER["HTTPS"]) ? 'https://' : 'http://';
	$company['images']=$a.$config_url.'/'._upload_hinhanh_l.$about_1['photo'];
	if (class_exists('breadcrumb')) {
		$bread = new breadcrumb();
		$bread->add(_trangchu,$http.$config_url);		
	}
	$data = array(
		array("tbl"=>"product_danhmuc","field"=>"id_danhmuc","source"=>"product","com"=>"san-pham","type"=>"san-pham"),
		array("tbl"=>"product","field"=>"id","source"=>"product","com"=>"san-pham","type"=>"san-pham"),
		array("tbl"=>"news","field"=>"id","source"=>"news","com"=>"blog-chia-se","type"=>"blog-chia-se"),
		array("tbl"=>"news_danhmuc","field"=>"id_danhmuc","source"=>"news","com"=>"blog-chia-se","type"=>"blog-chia-se"),
		array("tbl"=>"news","field"=>"id","source"=>"news","com"=>"chinh-sach","type"=>"chinh-sach"),
		array("tbl"=>"news","field"=>"id","source"=>"news","com"=>"quy-trinh","type"=>"quy-trinh"),
		array("tbl"=>"news","field"=>"id","source"=>"news","com"=>"bai-viet-ve-nam","type"=>"baiviet"),
		array("tbl"=>"about","field"=>"id","source"=>"about","com"=>"gioi-thieu","type"=>"about"),
		array("tbl"=>"about","field"=>"id","source"=>"about","com"=>"hop-tac-kinh-doanh","type"=>"hoptac"),
		array("tbl"=>"about","field"=>"id","source"=>"about","com"=>"huong-dan-mua-hang","type"=>"huongdanmuahang"),
		array("tbl"=>"about","field"=>"id","source"=>"about","com"=>"trai-nam","type"=>"trainam"),
	);
    if($com){
		foreach($data as $k=>$v){
			if(isset($com) && $v['tbl']!='info'){
				$d->query("select id from #_".$v['tbl']." where tenkhongdau='".$com."' and type='".$v['type']."' and hienthi=1");
				if($d->num_rows()>=1){
					$row = $d->fetch_array();
					$_GET[$v['field']] = $row['id'];
					$com = $v['com'];	
					break;
				}
			}
		}
    }
	switch($com)
	{
		case 'gioi-thieu':
			$type = "about";
			$title = _gioithieu;
			$title_cat = _gioithieu;
			$type_og = "article";
			$bread->add($title_cat,$http.$com);		
			$source = "about";
			$template = "about";
			break;
		case 'hop-tac-kinh-doanh':
			$type = "hoptac";
			$title = "Hợp tác kinh doanh";
			$title_cat = "Hợp tác kinh doanh";
			$type_og = "article";
			$bread->add($title_cat,$http.$com);
			$source = "about";
			$template = "about";
			break;
		case 'huong-dan-mua-hang':
			$type = "huongdanmuahang";
			$title = "Hướng dẫn mua hàng";
			$title_cat = "Hướng dẫn mua hàng";
			$bread->add($title_cat,$http.$com);
			$type_og = "article";
			$source = "about";
			$template = "about";
			break;
		case '':
		case 'index':
			$title = $company['title'];
			$title_cat = $company['title'];
			$type_og = "website";
			$source = "index";
			$template = "index";
			break;
		case 'hoi-dap':
			$type = "hoidap";
			$title = "Hỏi đáp";
			$title_cat = "Hỏi đáp";
			$bread->add($title_cat,$http.$com);
			$title_cat = "Hỏi đáp";
			$template = "hoidap";
			break;
		case 'ajax':
			$source = "ajax";
			break;		
		case 'blog-chia-se':
			$type = "blog-chia-se";
			$title = "Blog Chia sẻ";
			$title_cat = "Blog Chia sẻ";
			$bread->add($title_cat,$http.$com);
			$type_og = isset($_GET['id']) ? "article" : "object";
			$source = "news";
			$template = isset($_GET['id']) ? "news_detail" : "news";
			break;
		case 'quy-trinh':
			$type = "quy-trinh";
			$title = "Quy trình";
			$title_cat = "Quy trình";
			$bread->add($title_cat,$http.$com);
			$type_og = isset($_GET['id']) ? "article" : "object";
			$source = "news";
			$template = isset($_GET['id']) ? "news_detail" : "news";
			break;
		case 'chinh-sach':
			$type = "chinh-sach";
			$title = "Chính sách";
			$title_cat = "Chính sách";
			$type_og = isset($_GET['id']) ? "article" : "object";
			$source = "news";
			$template = isset($_GET['id']) ? "news_detail" : "news";
			break;
		case 'bai-viet-ve-nam':
			$type = "baiviet";
			$title = "Bài viết về nấm";
			$title_cat = "Bài viết về nấm";
			$type_og = isset($_GET['id']) ? "article" : "object";
			$source = "news";
			$template = isset($_GET['id']) ? "news_detail" : "news";
			break;
		case 'trai-nam':
			$type = "trainam";
			$title = "Trại nấm";
			$title_cat = "Trại nấm";
			$bread->add($title_cat,$http.$com);
			$type_og = "article";
			$source = "about";
			$template = "about";
			break;
		case 'lien-he':
			$type = "lienhe";
			$title = _lienhe;
			$title_cat = _lienhe;
			$bread->add($title_cat,$http.$com);
			$source = "contact";
			$template = "contact";
			break;
		case 'tim-kiem':
			$type = "san-pham";
			$title = _ketquatimkiem;
			$title_cat = _ketquatimkiem;
			$bread->add($title_cat,getCurrentPageURL());
			$type_og = isset($_GET['id']) ? "article" : "object";
			$source = "search";
			$template = "product";
			break;		
		case 'san-pham':
			$type = "san-pham";
			$title = _sanpham;
			$title_cat = _sanpham;
			$bread->add($title_cat,$http.$com);
			$type_og = isset($_GET['id']) ? "article" : "object";
			$source = "product";
			$template = isset($_GET['id']) ? "product_detail" : "product";
			break;
		case 'ngonngu':
			if(isset($_GET['lang']))
			{
				switch($_GET['lang'])
					{
						case '':
							$_SESSION['lang'] = '';
							break;
						case 'en':
							$_SESSION['lang'] = 'en';
							break;
						default: 
							$_SESSION['lang'] = '';
							break;
					}
			}
			else{
				$_SESSION['lang'] = '';
			}
		redirect($_SERVER['HTTP_REFERER']);
		break;						
		default:
			redirect($http.$config_url."/404.php");
	}
	if($config['index']==1){
		if($_SERVER["REQUEST_URI"]=='/index.php'){
			redirect($http.$config_url);
		}
	}
	if($source!="") include _source.$source.".php";	
	if($_REQUEST['com']=='logout')
	{
		session_unregister($login_name);
		header("Location:index.php");
	}
?>